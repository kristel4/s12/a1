//Excercise1
function numDiv(num) {
	if(num % 5 == 0 && num % 3 == 0) {
		return 'FizzBuzz';
	} else if (num % 3 == 0) {
		return "Fizz";
	} else if (num % 5 == 0) {
		return "Buzz";
	} else {
		return "Pop";
	}
}

console.log(numDiv(5));
console.log(numDiv(30));
console.log(numDiv(27));
console.log(numDiv(17));

//Exercise 2
function rainbow(letter){
	let letterCleaned = letter.toLowerCase();
	if (letterCleaned == "r"){
		return "Red";
	} else if (letterCleaned == "o"){
		return "Orange";
	} else if (letterCleaned == "y"){
		return "Yellow";
	} else if (letterCleaned == "g"){
		return "Green";
	} else if (letterCleaned == "b"){
		return "Blue";
	} else if (letterCleaned == "i"){
		return "Indigo";
	} else if (letterCleaned == "v"){
		return "Violet";
	} else {
		return "No Color";
	}
}

console.log(rainbow("r"));
console.log(rainbow("G"));
console.log(rainbow("x"));
console.log(rainbow("B"));
console.log(rainbow("i"));

//Exercise 3
function leapYear(year){
	if ((0 == year % 4) && (0 != year % 100) || (0 == year % 400)) {
		return "Leap Year";
	} else {
		return "Not a leap year";
	}
}

console.log(leapYear(1900));
console.log(leapYear(2000));
console.log(leapYear(2004));
console.log(leapYear(2021));
